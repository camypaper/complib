﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompLib.Graph.Tree {
    #region Centroid
    public class CentroidDecomposition {
        public bool[] marked;
        int[] size;
        public List<int>[] G;
        int N;

        public CentroidDecomposition(int n) {
            N = n;
            G = new List<int>[n];
            for (int i = 0; i < n; i++)
                G[i] = new List<int>();
            marked = new bool[n];
            size = new int[n];
        }
        public void AddEdge(int u, int v) {
            G[u].Add(v);
            G[v].Add(u);
        }
        int dfs(int prev, int cur) {
            size[cur] = 1;
            foreach (var t in G[cur])
                if (!marked[t] && t != prev)
                    size[cur] += dfs(cur, t);
            return size[cur];
        }
        void efs(int prev, int cur, int n, ref int c) {
            var ma = 0;
            foreach (var t in G[cur])
                if (!marked[t] && t != prev)
                {
                    ma = Math.Max(ma, size[t]);
                    efs(cur, t, n, ref c);
                }
            ma = Math.Max(ma, n - size[cur]);
            if (ma * 2 <= n) c = cur;
        }
        public int GetCenter(int v) {
            var s = dfs(-1, v);
            int c = -1;
            efs(-1, v, s, ref c);
            return c;
        }

    }
    #endregion
}
