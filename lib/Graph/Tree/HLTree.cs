﻿using System;
using System.Collections.Generic;

namespace CompLib.Graph.Tree {
    #region HLTree
    public class HLTree {
        int n;
        List<int>[] G;
        int[] subtreeSize;
        int[] maxsize;
        int[] par;
        int[] d;

        Chain[] go;
        int[] pos;
        List<Chain> chains;
        public HLTree(int N) {
            n = N;
            G = new List<int>[n];
            chains = new List<Chain>();
            for (int i = 0; i < n; i++)
                G[i] = new List<int>();
            subtreeSize = new int[n];
            maxsize = new int[n];
            par = new int[n];
            d = new int[n];
            go = new Chain[n];
            pos = new int[n];
        }
        public void AddEdge(int u, int v) {
            G[u].Add(v); G[v].Add(u);
        }
        public void Build(int root = 0) {
            computeSubtreeSize(root);
            Decomposite(-1, root, 0);

        }
        void computeSubtreeSize(int r) {
            var stack = new Stack<int>();
            stack.Push(r);
            var iter = new int[n];
            par[r] = -1;
            //dfs
            while (stack.Count > 0)
            {
                var cur = stack.Peek();
                if (iter[cur] == G[cur].Count)
                {
                    stack.Pop();
                    subtreeSize[cur]++;
                    if (par[cur] != -1)
                    {
                        subtreeSize[par[cur]] += subtreeSize[cur];
                        maxsize[par[cur]] = Math.Max(maxsize[par[cur]], subtreeSize[cur]);
                    }
                    continue;
                }
                var next = G[cur][iter[cur]++];
                if (next == par[cur]) continue;
                par[next] = cur;
                d[next] = d[cur] + 1;
                stack.Push(next);
            }

        }
        void Decomposite(int from, int cur, int lv) {
            var chain = new Chain(from, lv);
            chains.Add(chain);
            chain.Add(cur);
            while (cur != from)
            {
                var next = cur;
                go[cur] = chain;
                pos[cur] = chain.Count;
                foreach (var to in G[cur])
                {
                    if (to == from) continue;
                    if (maxsize[cur] == subtreeSize[to])
                    {
                        maxsize[cur] = -1;
                        next = to;
                        chain.Add(to);
                    }
                    else Decomposite(cur, to, lv + 1);
                }

                from = cur;
                cur = next;

            }
            chain.Init();

        }
        public int GetLCA(int u, int v) {
            while (go[u] != go[v])
            {
                if (go[u].Level < go[v].Level) v = go[v].From;
                else u = go[u].From;
            }
            if (d[u] <= d[v]) return u;
            else return v;
        }


    }
    public class Chain {
        const bool DEBUG = true;
        public int From { get; set; }
        public int Level { get; set; }
        public int Count { get; private set; }
        List<int> heavy;
        public Chain(int from, int lv) {
            From = from;
            Level = lv;
            heavy = new List<int>();
        }
        public void Add(int v) {
            if (DEBUG)
                heavy.Add(v);
            Count++;
        }
        public void Init() {

        }
    }
    #endregion
}
