﻿using System.Collections.Generic;

namespace CompLib.Graph.Undirected {
    #region Bridges
    public class BridgeGraph {
        int n;
        List<int> E;
        List<int>[] g;

        int[] used;
        int[] val;
        int[] group;

        /// <summary>
        /// 二重辺連結成分内に含まれる元の頂点
        /// </summary>
        public List<List<int>> Components = new List<List<int>>();
        public int Size { get { return Components.Count; } }
        public int this[int index] { get { return group[index]; } }


        /// <summary>
        /// 二重辺連結成分分解をするためのグラフをつくる．グラフが非連結なのを木にしたいときは仮の頂点を作っていい感じにやる
        /// </summary>
        public BridgeGraph(int N) {
            n = N;
            g = new List<int>[n];
            for (int i = 0; i < n; i++)
                g[i] = new List<int>();
            E = new List<int>();
        }
        public void AddEdge(int f, int t) {
            var id = E.Count;
            g[f].Add(id);
            g[t].Add(id);
            E.Add(f ^ t);
        }
        /// <summary>
        /// 二重辺連結成分分解をする
        /// </summary>
        /// <returns></returns>
        public void Build() {
            Decomposite();
            group = new int[n];
            for (int i = 0; i < Components.Count; i++)
                foreach (var x in Components[i]) group[x] = i;
        }
        /// <summary>
        /// 二重辺連結成分の個数を返す
        /// </summary>
        public int Decomposite() {
            used = new int[n];
            val = new int[n];
            var ptr = 0;
            for (int i = 0; i < n; i++)
                if (used[i] == 0) dfs(-1, i, ref ptr);
            used = new int[n];
            ptr = 0;
            for (int i = 0; i < n; i++)
                if (used[i] == 0) efs(-1, i, null, ref ptr);
            return Components.Count;
        }

        void dfs(int eid, int cur, ref int ptr) {
            used[cur] = ++ptr;
            foreach (var id in g[cur])
            {
                if (id == eid) continue;
                var t = E[id] ^ cur;
                if (used[t] != 0)
                {
                    if (used[t] < used[cur])
                    {
                        val[cur]++;
                        val[t]--;
                    }
                }
                else
                {
                    dfs(id, t, ref ptr);
                    val[cur] += val[t];
                }

            }

        }
        void efs(int eid, int cur, List<int> component, ref int ptr) {
            if (component == null)
            {
                component = new List<int>();
                Components.Add(component);
            }
            used[cur] = ++ptr;
            component.Add(cur);
            foreach (var id in g[cur])
            {
                if (id == eid) continue;
                var t = E[id] ^ cur;
                if (used[t] != 0) continue;
                if (val[t] == 0) efs(id, t, null, ref ptr);
                else efs(id, t, component, ref ptr);
            }
        }
        public bool IsSame(int u, int v) { return group[u] == group[v]; }

    }
    #endregion
}
