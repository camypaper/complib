﻿using System;
using System.Collections.Generic;
namespace CompLib.Graph.Flow {
    using C = System.Int32;
    #region Max Flow
    /// <summary>
    /// 辺の追加を行い，最大流を求めることが可能なグラフです．
    /// </summary>
    public class MaxFlow {
        readonly List<Edge>[] G;
        readonly int[] lv, iter;
        /// <summary>
        /// 頂点数 <paramref name="V"/> で辺が存在しないグラフを構築します．
        /// </summary>
        /// <param name="V">頂点の数</param>
        /// <remarks>この操作は O(<paramref name="V"/>) で実行されます．</remarks>
        public MaxFlow(int V) {
            G = new List<Edge>[V];
            lv = new int[V];
            iter = new int[V];
            for (int i = 0; i < G.Length; i++)
                G[i] = new List<Edge>();
        }

        /// <summary>
        /// 頂点 <paramref name="from"/> から頂点 <paramref name="to"/> への容量 <paramref name="cap"/> の有向辺を追加します．
        /// </summary>
        /// <param name="from">始点となる頂点の 0-indexed での番号</param>
        /// <param name="to">終点となる頂点の 0-indexed での番号</param>
        /// <param name="cap">辺の容量</param>
        /// <remarks>この操作は O(1) で実行されます．</remarks>
        public void AddDirectedEdge(int from, int to, C cap) {
            addEdge(from, to, cap, 0);
        }

        /// <summary>
        /// 頂点 <paramref name="from"/> から頂点 <paramref name="to"/> への容量 <paramref name="cap"/> の無向辺を追加します．
        /// </summary>
        /// <param name="from">始点となる頂点の 0-indexed での番号</param>
        /// <param name="to">終点となる頂点の 0-indexed での番号</param>
        /// <param name="cap">辺の容量</param>
        /// <remarks>この操作は O(1) で実行されます．</remarks>
        public void AddUndirectedEdge(int from, int to, C cap) {
            addEdge(from, to, cap, cap);
        }
        void addEdge(int f, int t, C c1, C c2) {
            var a = new Edge(t, c1);
            var b = new Edge(f, c2);
            Edge.Link(a, b);
            G[f].Add(a);
            G[t].Add(b);
        }


        /// <summary>
        /// 頂点 <paramref name="src"/> から頂点 <paramref name="sink"/> へ最大で流量 <paramref name="f"/> だけフローを流します．
        /// </summary>
        /// <param name="src">ソースとなる頂点の 0-indexed での番号</param>
        /// <param name="sink">シンクとなる頂点の 0-indexed での番号</param>
        /// <param name="f">ソースからシンクへと流す最大の容量．-1を指定した場合，最大流を求める．</param>
        /// <remarks>この操作はDinic法に基づき，最悪計算量 O(EV^2) で実行されます．Vは頂点の数，Eは辺の数ｗ表します．</remarks>
        public C Execute(int src, int sink, C f = -1) {
            C flow = 0;
            if (f < 0) f = C.MaxValue;
            while (f > 0)
            {
                bfs(src);
                if (lv[sink] == 0) return flow;
                Array.Clear(iter, 0, iter.Length);
                C df;
                while ((df = dfs(src, sink, f)) > 0) { flow += df; f -= df; }
            }
            return flow;
        }

        void bfs(int s) {
            Array.Clear(lv, 0, lv.Length);
            var q = new Queue<int>();
            lv[s] = 1;
            q.Enqueue(s);
            while (q.Count > 0)
            {
                var v = q.Dequeue();
                foreach (var e in G[v])
                    if (e.Cap > 0 && lv[e.To] == 0)
                    {
                        lv[e.To] = lv[v] + 1;
                        q.Enqueue(e.To);
                    }
            }

        }
        C dfs(int v, int t, C f) {
            if (v == t) return f;
            C ret = 0;
            for (; iter[v] < G[v].Count; iter[v]++)
            {
                var e = G[v][iter[v]];
                if (e.Cap <= 0 || lv[v] >= lv[e.To]) continue;
                C df = dfs(e.To, t, Math.Min(f, e.Cap));
                if (df <= 0) continue;
                e.Cap -= df;
                e.Rev.Cap += df;
                ret += df; f -= df;
                if (f == 0) break;
            }
            return ret;

        }
        class Edge {
            public static void Link(Edge e1, Edge e2) {
                e1.Rev = e2; e2.Rev = e1;
            }
            public int To { get; private set; }
            public Edge Rev { get; private set; }
            public C Cap { get; set; }
            public Edge(int t, C c) {
                To = t;
                Cap = c;
            }
            public override string ToString() {
                return string.Format("to: {0}, cap: {1}", To, Cap);
            }
        }


    }
    #endregion
}
