﻿using System;
using System.Collections.Generic;
using CompLib.Collections;
namespace CompLib.Graph.Flow
{
    using C = System.Int32;
    using V = System.Int32;
    #region Mincost Flow
    /// <summary>
    /// 辺の追加を行い，最小費用流を求めることが可能なグラフです．
    /// </summary>
    public class MinCostFlow
    {
        bool hasng;
        readonly List<Edge>[] G;
        /// <summary>
        /// 頂点数 <paramref name="V"/> で辺が存在しないグラフを構築します．
        /// </summary>
        /// <param name="V">頂点の数</param>
        /// <remarks>この操作は O(<paramref name="V"/>) で実行されます．</remarks>
        public MinCostFlow(int V)
        {
            G = new List<Edge>[V];
            for (int i = 0; i < V; i++)
                G[i] = new List<Edge>();

        }
        /// <summary>
        /// 頂点 <paramref name="from"/> から頂点 <paramref name="to"/> への容量 <paramref name="cap"/> ，コスト <paramref name="cost"/> の有向辺を追加します．
        /// </summary>
        /// <param name="from">始点となる頂点の 0-indexed での番号</param>
        /// <param name="to">終点となる頂点の 0-indexed での番号</param>
        /// <param name="cap">辺の容量</param>
        /// <param name="cost">辺のコスト</param>
        /// <remarks>この操作は O(1) で実行されます．</remarks>
        public void AddDirectedEdge(int from, int to, C cap, V cost)
        {
            addEdge(from, to, cap, 0, cost);
        }

        /// <summary>
        /// 頂点 <paramref name="src"/> から頂点 <paramref name="sink"/> へ最大で流量 <paramref name="f"/> だけ最小費用フローを流します．
        /// </summary>
        /// <param name="src">ソースとなる頂点の 0-indexed での番号</param>
        /// <param name="sink">シンクとなる頂点の 0-indexed での番号</param>
        /// <param name="f">ソースからシンクへと流す最大の容量．-1を指定した場合，最小費用最大流を求める．</param>
        /// <param name="inf">ソースからシンクまでの距離の最大値</param>
        /// <remarks>この操作はPrimal Dual法に基づき，最悪計算量 O(VE + FE log V) で実行されます．Vは頂点の数，Eは辺の数，Fは流量を表します．</remarks>
        public KeyValuePair<C, V> Execute(int src, int sink, C f = C.MaxValue, V inf = V.MaxValue / 3)
        {
            var n = G.Length;
            var dist = new V[n];
            var prev = new int[n];
            var prevEdge = new Edge[n];
            var potential = new V[n];


            C flow = 0;
            V cost = 0;
            var first = hasng;
            while (f > 0)
            {
                for (int i = 0; i < G.Length; i++)
                    dist[i] = inf;

                //shortest path
                if (first)//spfa
                {
                    var q = new Queue<int>();
                    q.Enqueue(src); dist[src] = 0;
                    var inQ = new bool[n];
                    while (q.Count > 0)
                    {
                        var p = q.Dequeue();
                        inQ[p] = false;
                        foreach (var e in G[p])
                        {
                            var t = e.To;
                            var d = dist[p] + e.Cost;
                            if (e.Cap > 0 && d < dist[t])
                            {
                                if (!inQ[t])
                                {
                                    inQ[t] = true;
                                    q.Enqueue(t);
                                }
                                dist[t] = d; prev[t] = p; prevEdge[t] = e;
                            }
                        }
                    }
                    first = false;
                }
                else//dijkstra
                {
                    var vis = new bool[n];
                    var pq = new PriorityQueue<KeyValuePair<int, V>>((l, r) => l.Value.CompareTo(r.Value));
                    pq.Enqueue(new KeyValuePair<int, V>(src, 0));
                    dist[src] = 0;
                    while (pq.Count > 0)
                    {
                        var p = pq.Dequeue().Key;
                        if (vis[p]) continue;
                        vis[p] = true;
                        foreach (var e in G[p])
                        {
                            if (e.Cap <= 0) continue;
                            var t = e.To;
                            if (vis[t]) continue;
                            var d = dist[p] + e.Cost + potential[p] - potential[t];
                            if (dist[t] > d)
                            {
                                dist[t] = d; prev[t] = p; prevEdge[t] = e;
                                pq.Enqueue(new KeyValuePair<int, V>(t, d));
                            }
                        }
                    }


                }


                //update
                {
                    if (dist[sink] == inf) break;
                    for (int i = 0; i < n; i++)
                        potential[i] = Math.Min(inf, potential[i] + dist[i]);
                    C df = f;
                    V d = 0;
                    for (var v = sink; v != src; v = prev[v])
                    {
                        var e = prevEdge[v];
                        df = Math.Min(df, e.Cap); d += e.Cost;
                    }
                    f -= df; cost += df * d; flow += df;
                    for (var v = sink; v != src; v = prev[v])
                    {
                        var e = prevEdge[v];
                        e.Cap -= df; e.Rev.Cap += df;
                    }
                }

            }
            return new KeyValuePair<C, V>(flow, cost);
        }
        void addEdge(int f, int t, C c1, C c2, V e)
        {
            if (e < 0) hasng = true;
            var a = new Edge(t, c1, e);
            var b = new Edge(f, c2, -e);
            Edge.Link(a, b);
            G[f].Add(a);
            G[t].Add(b);
        }
        class Edge
        {
            public static void Link(Edge e1, Edge e2)
            {
                e1.Rev = e2; e2.Rev = e1;
            }
            public int To { get; private set; }
            public Edge Rev { get; private set; }
            public V Cost { get; private set; }
            public C Cap { get; set; }
            public Edge(int t, C c, V e)
            {
                To = t;
                Cap = c;
                Cost = e;
            }
            public override string ToString()
            {
                return string.Format("to: {0}, cap: {1}", To, Cap);
            }
        }
    }
    #endregion
}
