using System.Collections.Generic;

namespace CompLib.Graph.Directed {
    #region SCC
    public class SCCGraph {
        int n;

        List<int>[] g;
        List<List<int>> scc;
        int[] group;

        /// <summary>
        /// 強連結成分のサイズ
        /// </summary>
        public int Count { get; private set; }

        public SCCGraph(int N) {
            n = N;
            g = new List<int>[n];
            scc = new List<List<int>>();
            for (int i = 0; i < n; i++)
                g[i] = new List<int>();
        }
        public void AddEdge(int f, int t) { g[f].Add(t); }
        public int this[int id] { get { return group[id]; } }
        public void Build() {
            Decomposite();
            scc.Reverse();
            group = new int[n];
            for (int i = 0; i < Count; i++)
                foreach (var x in scc[i]) group[x] = i;

        }
        /// <summary>
        /// 強連結成分分解をする
        /// </summary>
        public int Decomposite() {
            var S = new Stack<int>(n + 2);
            var B = new Stack<int>(n + 2);
            var I = new int[n];
            var iter = new int[n];
            var s = new Stack<int>(n + 2);
            for (int i = 0; i < n; i++)
            {
                if (I[i] != 0) continue;
                s.Push(i);
                while (s.Count > 0)
                {
                    DFS:
                    var u = s.Peek();
                    if (I[u] == 0)
                    {
                        B.Push(I[u] = S.Count);
                        S.Push(u);
                        iter[u] = 0;
                    }
                    while (iter[u] < g[u].Count)
                    {
                        var v = g[u][iter[u]++];
                        if (I[v] == 0)
                        {
                            s.Push(v);
                            goto DFS;
                        }
                        else while (I[v] < B.Peek()) B.Pop();
                    }
                    if (I[u] == B.Peek())
                    {
                        var ns = new List<int>();
                        scc.Add(ns);
                        B.Pop();
                        while (I[u] < S.Count)
                        {
                            var p = S.Pop();
                            ns.Add(p);
                            I[p] = n + scc.Count;
                        }
                    }
                    s.Pop();

                }
            }
            return Count = scc.Count;

        }
        public bool IsSameGroup(int u, int v) { return group[u] == group[v]; }
    }
    #endregion
}
