namespace CompLib.Collections {

    #region Disjoint Set
    /// <summary>
    /// いくつかの要素を素集合に分割して保持するコレクションです．
    /// </summary>
    public class DisjointSet {
        int[] par;
        byte[] rank;
        /// <summary>
        /// 0 から <paramref name="N"/>-1 までの番号がついた <paramref name="N"/> 個の要素からなる集合を作成します．
        /// </summary>
        /// <param name="N">要素数</param>
        /// <remarks>このコンストラクタは O(N) で実行されます．</remarks>
        public DisjointSet(int N) {
            par = new int[N];
            rank = new byte[N];
            for (int i = 0; i < N; i++)
                par[i] = -1;
        }
        /// <summary>
        /// 指定した要素が属する集合の代表値を取得します．
        /// </summary>
        /// <param name="id">調べたい要素の 0-indexed での番号</param>
        /// <returns>指定した要素が属する集合の代表値</returns>
        /// <remarks>最悪計算量 O(α(N)) で実行されます．</remarks>
        public int this[int id] {
            get {
                if ((par[id] < 0)) return id;
                return par[id] = this[par[id]];
            }
        }
        /// <summary>
        /// 指定した 2 つの要素が属する集合同士を 1 つに統合することを試みます．
        /// </summary>
        /// <param name="x">最初の要素の 0-indexed での番号</param>
        /// <param name="y">2 つ目の要素の 0-indexed での番号</param>
        /// <returns>統合に成功したならば true，そうでなければ false．</returns>
        /// <remarks>最悪計算量 O(α(N)) で実行されます．</remarks>
        public bool Unite(int x, int y) {
            x = this[x]; y = this[y];
            if (x == y) return false;
            if (rank[x] < rank[y]) { var tmp = x; x = y; y = tmp; }
            par[x] += par[y];
            par[y] = x;
            if (rank[x] == rank[y])
                rank[x]++;
            return true;
        }
        /// <summary>
        /// 指定した要素が属する集合のサイズを取得します．
        /// </summary>
        /// <param name="x">指定する要素の 0-indexed での番号</param>
        /// <returns>集合のサイズ</returns>
        /// <remarks>最悪計算量 O(α(N)) で実行されます．</remarks>
        public int Size(int x) { return -par[this[x]]; }

        /// <summary>
        /// 指定した 2 つの要素が属する集合が同じかどうかを判定する．計算量 O(α(N))．
        /// </summary>
        /// <param name="x">最初の要素の 0-indexed での番号</param>
        /// <param name="y">2 つ目の要素の 0-indexed での番号</param>
        /// <returns>同じならば true，そうでなければ false．</returns>
        /// <remarks>最悪計算量 O(α(N)) で実行されます．</remarks>
        public bool Same(int x, int y) { return this[x] == this[y]; }

    }
    #endregion
}
