﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompLib.Algorithms {
    #region sat solver
    public class SatSolver {
        List<List<KeyValuePair<int, bool>>> cl;
        Dictionary<KeyValuePair<int, bool>, List<int>> w;
        int[] reason, level, activity;
        List<int> que;
        bool[] assigns;
        int n, qi;

        public SatSolver(int N) {
            cl = new List<List<KeyValuePair<int, bool>>>();
            w = new Dictionary<KeyValuePair<int, bool>, List<int>>();
            reason = new int[N];
            level = new int[N];
            activity = new int[N];
            que = new List<int>();
            assigns = new bool[N];
            n = N;

        }
        public bool Solve() {
            while (true)
            {
                var confl = propagate();
                if (confl != -1)
                {
                    if (level[que[que.Count - 1]] == 1) return false;
                    for (int i = 0; i < activity.Length; i++)
                        activity[i] = (int)(activity[i] / 1.05);
                    analyze(confl);
                }
                else
                {
                    var k = -1;
                    for (int i = 0; i < n; i++)
                        if (level[i] == 0 && (k == -1 || activity[k] < activity[i])) k = i;
                    if (k == -1) return true;
                    enqueue(k, assigns[k]);
                    level[k]++;
                }
            }
        }
        public bool this[int index] {
            get { return assigns[index]; }
        }

        void enqueue(int v, bool a, int r = -1) {
            assigns[v] = a;
            reason[v] = r;
            level[v] = (que.Count == 0) ? 1 : level[que[que.Count - 1]];
            que.Add(v);
        }
        /// <summary>
        /// add clause ((variable ,true or not) v ... v (variable ,true or not))
        /// </summary>
        public void Add(List<KeyValuePair<int, bool>> clause) {
            foreach (var l in clause)
            {
                List<int> a;
                if (w.TryGetValue(l, out a)) a.Add(cl.Count);
                else w.Add(l, new List<int>() { cl.Count });
            }
            cl.Add(clause);
        }
        void analyze(int confl) {
            int i = que.Count, lv = 1;
            var used = new HashSet<int>();
            var learnt = new List<KeyValuePair<int, bool>>();
            for (int cnt = 0; cnt > 0 || used.Count == 0; cnt--)
            {
                foreach (var q in cl[confl])
                {
                    if (!used.Add(q.Key)) continue;
                    activity[q.Key] += 100000;
                    if (level[q.Key] == level[que[que.Count - 1]]) cnt++;
                    else
                    {
                        learnt.Add(q);
                        lv = Math.Max(lv, level[q.Key]);
                    }
                }
                while (!used.Contains(que[--i])) ;
                confl = reason[que[i]];
            }
            learnt.Add(new KeyValuePair<int, bool>(que[i], !assigns[que[i]]));
            for (; que.Count != 0 && level[que[que.Count - 1]] > lv; que.RemoveAt(que.Count - 1)) level[que[que.Count - 1]] = 0;
            qi = que.Count;
            enqueue(learnt[learnt.Count - 1].Key, learnt[learnt.Count - 1].Value, cl.Count);
            Add(learnt);
        }

        int propagate() {
            for (; qi < que.Count; qi++)
            {
                List<int> a;
                var key = new KeyValuePair<int, bool>(que[qi], !assigns[que[qi]]);
                if (w.TryGetValue(key, out a)) { }
                else w.Add(key, new List<int>());
                foreach (var cr in w[new KeyValuePair<int, bool>(que[qi], !assigns[que[qi]])])
                {
                    var cnt = 0;
                    for (int i = 0; i < cl[cr].Count; i++)
                    {
                        var lit = cl[cr][i];
                        if (level[lit.Key] == 0)
                        {
                            activity[lit.Key] += 1000;
                            //swap(lit,cl[cr][0])
                            var tmp = lit; cl[cr][i] = cl[cr][0]; cl[cr][0] = lit;
                            cnt++;
                        }
                        else if (assigns[lit.Key] == lit.Value)
                        {
                            //swap(lit,cl[cr][0])
                            var tmp = lit; cl[cr][i] = cl[cr][0]; cl[cr][0] = lit;
                            cnt = -1; break;
                        }
                    }
                    if (cnt == 0) return cr;
                    if (cnt == 1) enqueue(cl[cr][0].Key, cl[cr][0].Value, cr);
                }
            }
            return -1;
        }

    }
    #endregion
}
