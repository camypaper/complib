﻿namespace CompLib.Strings {
    #region KMP
    static public partial class StringEx {
        /// <summary>KMPテーブルを構築する．O(|S|)</summary>
        /// <returns>ret[i]=s[0,i-1]の接頭辞と接尾辞の最大一致数</returns>
        static public int[] CalcKMP(string s) {
            var ret = new int[s.Length + 1]; ret[0] = -1;
            int j = -1;
            for (int i = 0; i < s.Length;)
            {
                while (j >= 0 && s[i] != s[j]) j = ret[j];
                i++; j++;
                if (i < s.Length && s[i] == s[j]) ret[i] = ret[j];
                else ret[i] = j;
            }
            return ret;
        }

        /// <summary>
        /// i文字一致しているときに c をくっつけたときに，どこにいくか？
        /// O(N|C|logN)
        /// </summary>
        /// <returns>ret[i][c]=s[0,i)+cはどこに行くべきか？</returns>
        static public int[][] GetLink(string s) {
            var kmp = CalcKMP(s);
            var ret = new int[s.Length][];
            for (int i = 0; i < s.Length; i++)
                ret[i] = new int[26];
            for (int i = 0; i < s.Length; i++)
                for (int c = 0; c < 26; c++)
                {
                    var j = i;
                    while (j >= 0 && c != s[j] - 'a') j = kmp[j];
                    ret[i][c] = j + 1;
                }
            return ret;
        }
    }
    #endregion
}
