﻿namespace CompLib.Strings {
    #region Manacher
    static public partial class StringEx {
        /// <summary>s[i]を中心とする最長回文の半径を求める．O(|S|)</summary>
        /// <returns>R[i]=iを中心とする最長回文の半径</returns>
        static public int[] Manacher(string s) {
            var R = new int[s.Length];
            int i = 0, j = 0;
            while (i < s.Length)
            {
                while (i - j >= 0 && i + j < s.Length && s[i - j] == s[i + j]) ++j;
                R[i] = j;
                int k = 1;
                while (i - k >= 0 && i + k < s.Length && k + R[i - k] < j) { R[i + k] = R[i - k]; ++k; }
                i += k; j -= k;
            }
            return R;
        }
        /// <summary>s[i]を中心とする最長回文の半径を(偶数長のものも含めて)求める．O(|S|)</summary>
        /// <returns>ret[i]=i/2を中心とする最長回文の半径．iが奇数のときは偶数長</returns>
        static public int[] ManacherWithEven(string s) {
            var sb = new System.Text.StringBuilder();
            foreach (var x in s)
            {
                sb.Append(x);
                sb.Append('$');
            }
            var t = sb.ToString();
            var ret = Manacher(t);
            for (int i = 1; i < ret.Length; i += 2) ret[i] = ret[i] / 2;
            var ret2 = Manacher(s);
            for (int i = 0; i < ret.Length; i += 2) ret[i] = ret2[i / 2];
            return ret;
        }
    }
    #endregion
}
