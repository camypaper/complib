﻿using System;

namespace CompLib.Mathematics {
    using N = System.Int32;
    #region Matrix
    public class Matrix {
        int row, col;
        public N[] mat;
        /// <summary>
        /// <paramref name="r"/> 行 <paramref name="c"/> 列目の要素へのアクセスを提供します。
        /// </summary>
        /// <param name="r">行の番号</param>
        /// <param name="c">列の番号</param>
        public N this[int r, int c] {
            get { return mat[r * col + c]; }
            set { mat[r * col + c] = value; }
        }
        public Matrix(int r, int c) {
            row = r; col = c;
            mat = new N[row * col];
        }
        public static Matrix operator *(Matrix l, Matrix r) {
            System.Diagnostics.Debug.Assert(l.col == r.row);
            var ret = new Matrix(l.row, r.col);
            for (int i = 0; i < l.row; i++)
                for (int k = 0; k < l.col; k++)
                    for (int j = 0; j < r.col; j++)
                        ret.mat[i * r.col + j] = (ret.mat[i * r.col + j] + l.mat[i * l.col + k] * r.mat[k * r.col + j]);
            return ret;
        }
        /// <summary>
        /// <paramref name="m"/>^<paramref name="n"/> を O(<paramref name="m"/>^3 log <paramref name="n"/>) で計算します。
        /// </summary>
        public static Matrix Pow(Matrix m, long n) {
            var ret = new Matrix(m.row, m.col);
            for (int i = 0; i < m.row; i++)
                ret.mat[i * m.col + i] = 1;
            for (; n > 0; m *= m, n >>= 1)
                if ((n & 1) == 1)
                    ret = ret * m;
            return ret;

        }
        public N[][] Items {
            get {
                var a = new N[row][];
                for (int i = 0; i < row; i++)
                {
                    a[i] = new N[col];
                    for (int j = 0; j < col; j++)
                        a[i][j] = mat[i * col + j];
                }
                return a;
            }
        }
    }
    #endregion
}
