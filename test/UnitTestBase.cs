﻿using Xunit.Abstractions;
namespace CompLib.UnitTest
{
    public abstract class UnitTestBase
    {

        protected UnitTestBase(ITestOutputHelper output)
        {
            Output = output;
            Rand = new XRand();
        }
        public ITestOutputHelper Output { get; }
        public XRand Rand { get; }

        protected long ten(int n) => n == 0 ? 1 : 10 * ten(n - 1);
    }
}
