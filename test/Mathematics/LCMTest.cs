﻿using System.Numerics;
using Xunit;
using Xunit.Abstractions;
using CompLib.Mathematics;
namespace CompLib.UnitTest.Mathematics
{
    public class LCMUnitTest: UnitTestBase
    {
        public LCMUnitTest(ITestOutputHelper output) : base(output)
        {
        }

        [Trait("Category", "Math")]
        [Trait("Category", "LCM")]
        [Theory(DisplayName = "lcm(x,y), 1 <= x,y <=10^9+9")]
        [
            InlineData(1, 1),
            InlineData(2, 3),
            InlineData(999999929, 999999937),
            InlineData(100, 100),
            InlineData(800, 65)
        ]
        public void LCMTest(int x, int y)
        {
            Assert.True(1 <= x && x <= ten(9));
            Assert.True(1 <= y && y <= ten(9));
            var expected = BigInteger.Multiply(x, y) / BigInteger.GreatestCommonDivisor(x, y);
            var actual = MathEx.LCM(x, y);
            Output.WriteLine($"lcm({x},{y}): expected: {expected}, actual: {actual}");
            Assert.Equal(expected, actual);
        }
        [Trait("Category", "Math")]
        [Trait("Category", "LCM")]
        [Fact(DisplayName = "lcm(x,y), T = 1000, 1<= x,y <= 10^9")]
        public void LCMRandomTest()
        {
            const int T = 1000;
            for (int i = 0; i < T; i++)
            {
                var x = Rand.NextInt(1, 9);
                var y = Rand.NextInt(1, 9);
                x = Rand.NextInt((int)ten(x - 1), (int)ten(x));
                y = Rand.NextInt((int)ten(y - 1), (int)ten(y));
                LCMTest(x, y);
            }
            for (int i = 0; i < T; i++)
            {
                var k = Rand.NextInt(1, 8);
                var x = Rand.NextInt(1, 9 - k);
                var y = Rand.NextInt(1, 9 - k);
                var gcd = Rand.NextInt((int)ten(k - 1), (int)ten(k));
                x = gcd * Rand.NextInt((int)ten(x - 1), (int)ten(x));
                y = gcd * Rand.NextInt((int)ten(y - 1), (int)ten(y));
                LCMTest(x, y);
            }
        }
    }
}
