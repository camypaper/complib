﻿using Xunit;
using Xunit.Abstractions;
using CompLib.Mathematics;
namespace CompLib.UnitTest.Mathematics
{
    public class PrimeUnitTest: UnitTestBase
    {
        public PrimeUnitTest(ITestOutputHelper output) : base(output)
        {
        }
        [Trait("Category", "Math")]
        [Trait("Category", "Prime Sieve")]
        [Theory(DisplayName ="isPrime(i), for all 1 <= i <= max")]
        [InlineData(1)]
        [InlineData(1000)]
        [InlineData(100000)]
        [InlineData(1000000)]
        public void PrimeTest(int max)
        {
            Assert.True(1 <= max && max <= 1000000);
            var expected = new bool[max + 1];
            for (int i = 2; i < expected.Length; i++)
                expected[i] = true;
            for (int i = 2; i * i <= max; i++)
                if (expected[i])
                {
                    for (int j = i * i; j <= max; j += i) expected[j] = false;
                }
            var actual = MathEx.Sieve(max);
            for (int i = 1; i <= max; i++)
                Assert.Equal(expected[i], actual[i]);
        }
    }
}
