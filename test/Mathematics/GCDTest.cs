﻿using System.Numerics;
using Xunit;
using Xunit.Abstractions;
using static System.Math;
using CompLib.Mathematics;
namespace CompLib.UnitTest.Mathematics
{
    public class GCDUnitTest: UnitTestBase
    {
        public GCDUnitTest(ITestOutputHelper output) : base(output)
        {

        }
        [Trait("Category", "Math")]
        [Trait("Category", "GCD")]
        [Theory(DisplayName = "gcd(x,y), |x|,|y|<=10^9")]
        [
            InlineData(1, 1),
            InlineData(4, 6),
            InlineData(0, 0),
            InlineData(0, 101),
            InlineData(-4, 6),
            InlineData(-4, -6),
            InlineData(1000000000, 1000000000)
        ]
        public void IntGCDTest(int n, int m)
        {
            Assert.True(Abs(n) <= ten(9));
            Assert.True(Abs(m) <= ten(9));
            var expected = (int)BigInteger.GreatestCommonDivisor(n, m);
            var actual = MathEx.GCD(n, m);
            Output.WriteLine($"gcd({n},{m}): expected: {expected}, actual: {actual}");
            Assert.Equal(expected, actual);

        }

        [Trait("Category", "Math")]
        [Trait("Category", "GCD")]
        [Theory(DisplayName = "gcd(x,y), |x|,|y|<=10^18")]
        [
            InlineData(1, 1),
            InlineData(4, 6),
            InlineData(0, 0),
            InlineData(0, 101),
            InlineData(-4, 6),
            InlineData(-4, -6),
            InlineData(1000000000000000000L, 1000000000000000000L)
        ]
        public void LongGCDTest(long n, long m)
        {
            Assert.True(Abs(n) <= ten(18));
            Assert.True(Abs(m) <= ten(18));
            var expected = (long)BigInteger.GreatestCommonDivisor(n, m);
            var actual = MathEx.GCD(n, m);
            Output.WriteLine($"gcd({n},{m}): expected: {expected}, actual: {actual}");
            Assert.Equal(expected, actual);

        }
        [Trait("Category", "Math")]
        [Trait("Category", "GCD")]
        [Fact(DisplayName = "gcd(x,y), T=1000, |x|,|y|<=10^9")]
        public void IntRandomGCDTest()
        {
            const int SIZE = 1000;
            for (int i = 0; i < SIZE; i++)
            {
                var x = Rand.NextInt(1, 9);
                var y = Rand.NextInt(1, 9);
                x = Rand.NextInt((int)ten(x - 1), (int)ten(x));
                y = Rand.NextInt((int)ten(y - 1), (int)ten(y));
                if (Rand.NextDouble() < 0.5) x = -x;
                if (Rand.NextDouble() < 0.5) y = -y;
                IntGCDTest(x, y);
            }
            for (int i = 0; i < SIZE; i++)
            {
                var k = Rand.NextInt(1, 8);
                var x = Rand.NextInt(1, 9 - k);
                var y = Rand.NextInt(1, 9 - k);
                var gcd = Rand.NextInt((int)ten(k - 1), (int)ten(k));
                x = gcd * Rand.NextInt((int)ten(x - 1), (int)ten(x));
                y = gcd * Rand.NextInt((int)ten(y - 1), (int)ten(y));
                if (Rand.NextDouble() < 0.5) x = -x;
                if (Rand.NextDouble() < 0.5) y = -y;
                IntGCDTest(x, y);
            }
        }
        [Trait("Category", "Math")]
        [Trait("Category", "GCD")]
        [Fact(DisplayName = "gcd(x,y), T=1000, |x|,|y|<=10^18")]
        public void LongRandomGCDTest()
        {
            const int SIZE = 1000;
            for (int i = 0; i < SIZE; i++)
            {
                var x = Rand.NextInt(1, 18);
                var y = Rand.NextInt(1, 18);
                var u = Rand.NextLong(ten(x - 1), ten(x));
                var v = Rand.NextLong(ten(y - 1), ten(y));
                if (Rand.NextDouble() < 0.5) u = -u;
                if (Rand.NextDouble() < 0.5) v = -v;
                LongGCDTest(u, v);
            }
            for (int i = 0; i < SIZE; i++)
            {
                var k = Rand.NextInt(1, 17);
                var x = Rand.NextInt(1, 18 - k);
                var y = Rand.NextInt(1, 18 - k);
                var gcd = Rand.NextLong(ten(k - 1), ten(k));
                var u = gcd * Rand.NextLong(ten(x - 1), ten(x));
                var v = gcd * Rand.NextLong(ten(y - 1), ten(y));
                if (Rand.NextDouble() < 0.5) u = -u;
                if (Rand.NextDouble() < 0.5) v = -v;
                LongGCDTest(u, v);
            }

        }
    }


}
