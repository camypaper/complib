﻿using Xunit;
using Xunit.Abstractions;
using System.Numerics;
using CompLib.Mathematics;
namespace CompLib.UnitTest.Mathematics
{
    public class ModIntTest: UnitTestBase
    {
        public ModIntTest(ITestOutputHelper output) : base(output)
        {
        }

        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Theory(DisplayName = "x + y (mod M), 0 <= x,y < M")]
        [
            InlineData(0, 0),
            InlineData(0, 1000),
            InlineData(1, 2),
            InlineData(7, 1000000000),
            InlineData(1000000006, 1),
            InlineData(1000000006, 1000000006)
        ]
        public void AddTest(int x, int y)
        {
            Assert.True(0 <= x && x < ModInt.Mod);
            Assert.True(0 <= y && y < ModInt.Mod);
            var expected = (x + y) % ModInt.Mod;
            var actual = new ModInt(x) + new ModInt(y);
            Output.WriteLine($"{x} + {y} (mod {ModInt.Mod}), expected: {expected}, actual: {actual}");
            Assert.Equal(expected, actual.num);
        }
        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Fact(DisplayName = "x + y (mod M), T = 1000, 1 <= x,y < M")]
        public void AddRandomTest()
        {
            const int T = 1000;
            for (int i = 0; i < T; i++)
            {
                var x = Rand.NextInt(1, 9);
                var y = Rand.NextInt(1, 9);
                x = Rand.NextInt((int)ten(x - 1), (int)ten(x) + 6);
                y = Rand.NextInt((int)ten(y - 1), (int)ten(y) + 6);
                AddTest(x, y);
            }
        }

        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Theory(DisplayName = "x - y (mod M), 0 <= x,y < M")]
        [
            InlineData(0, 0),
            InlineData(0, 1000),
            InlineData(1, 2),
            InlineData(7, 1000000000),
            InlineData(1000000006, 1),
            InlineData(1000000006, 1000000006)
        ]
        public void SubTest(int x, int y)
        {
            Assert.True(0 <= x && x < ModInt.Mod);
            Assert.True(0 <= y && y < ModInt.Mod);
            var expected = (x - y) % ModInt.Mod;
            if (expected < 0) expected += ModInt.Mod;
            var actual = new ModInt(x) - new ModInt(y);
            Output.WriteLine($"{x} - {y} (mod {ModInt.Mod}), expected: {expected}, actual: {actual}");
            Assert.Equal(expected, actual.num);
        }
        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Fact(DisplayName = "x - y (mod M), T = 1000, 1 <= x,y < M")]
        public void SubRandomTest()
        {
            const int T = 1000;
            for (int i = 0; i < T; i++)
            {
                var x = Rand.NextInt(1, 9);
                var y = Rand.NextInt(1, 9);
                x = Rand.NextInt((int)ten(x - 1), (int)ten(x) + 6);
                y = Rand.NextInt((int)ten(y - 1), (int)ten(y) + 6);
                SubTest(x, y);
            }
        }


        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Theory(DisplayName = "x * y (mod M), 0 <= x,y < M")]
        [
            InlineData(0, 0),
            InlineData(0, 1000),
            InlineData(1, 2),
            InlineData(7, 1000000000),
            InlineData(1000000006, 1),
            InlineData(1000000006, 1000000006)
        ]
        public void MulTest(int x, int y)
        {
            Assert.True(0 <= x && x < ModInt.Mod);
            Assert.True(0 <= y && y < ModInt.Mod);
            var expected = 1L * x * y % ModInt.Mod;
            var actual = new ModInt(x) * new ModInt(y);
            Output.WriteLine($"{x} * {y} (mod {ModInt.Mod}), expected: {expected}, actual: {actual}");
            Assert.Equal(expected, actual.num);
        }
        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Fact(DisplayName = "x * y (mod M), T = 1000, 1 <= x,y < M")]
        public void MulRandomTest()
        {
            const int T = 1000;
            for (int i = 0; i < T; i++)
            {
                var x = Rand.NextInt(1, 9);
                var y = Rand.NextInt(1, 9);
                x = Rand.NextInt((int)ten(x - 1), (int)ten(x) + 6);
                y = Rand.NextInt((int)ten(y - 1), (int)ten(y) + 6);
                MulTest(x, y);
            }
        }

        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Theory(DisplayName = "pow(x, y) (mod M), 0 <= x< M, 0<= y < 10^18")]
        [
            InlineData(0, 1000000000),
            InlineData(1, 100000),
            InlineData(7, 1000000000),
            InlineData(12345, 1000000000000000000),
            InlineData(123, 1000000006 * 100000000L)
        ]
        public void PowTest(int x, long y)
        {
            Assert.True(0 <= x && x < ModInt.Mod);
            Assert.True(0 <= y && y <= ten(18));
            var expected = BigInteger.ModPow(x, y, ModInt.Mod);
            var actual = ModInt.Pow(new ModInt(x), y);
            Output.WriteLine($"pow({x}, {y}) (mod {ModInt.Mod}), expected: {expected}, actual: {actual}");
            Assert.Equal(expected, actual.num);
        }
        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Fact(DisplayName = "pow(x, y) (mod M), T = 1000, 0 <= x< M, 0<= y < 10^18")]
        public void PowRandomTest()
        {
            const int T = 1000;
            for (int i = 0; i < T; i++)
            {
                var x = Rand.NextInt(1, 9);
                var y = Rand.NextInt(1, 18);
                x = Rand.NextInt((int)ten(x - 1), (int)ten(x) + 6);
                long k = Rand.NextLong(ten(y - 1), ten(y));
                PowTest(x, k);
            }
        }

        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Theory(DisplayName = "inv(x) (mod M), 0 <= x< M")]
        [
            InlineData(0),
            InlineData(1),
            InlineData(12345),
            InlineData(1000000000)
        ]
        public void InverseTest(int x)
        {
            Assert.True(0 <= x && x < ModInt.Mod);
            var expected = BigInteger.ModPow(x, ModInt.Mod - 2, ModInt.Mod);
            var actual = ModInt.Inverse(new ModInt(x));
            Output.WriteLine($"inv({x}) (mod {ModInt.Mod}), expected: {expected}, actual: {actual}");
            Assert.Equal(expected, actual.num);
        }
        [Trait("Category", "Math")]
        [Trait("Category", "ModInt")]
        [Fact(DisplayName = "inv(x) (mod M), T = 1000, 0 <= x< M")]
        public void InverseRandomTest()
        {
            const int T = 1000;
            for (int i = 0; i < T; i++)
            {
                var x = Rand.NextInt(1, 9);
                x = Rand.NextInt((int)ten(x - 1), (int)ten(x) + 6);
                InverseTest(x);
            }
        }

    }
}
