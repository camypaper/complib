﻿using static System.Math;
namespace CompLib.UnitTest
{
    public class XRand
    {
        uint x, y, z, w;
        public XRand()
        {
            x = 123456789; y = 362436069; z = 521288629; w = 88675123;
        }


        uint next() { uint t = x ^ (x << 11); x = y; y = z; z = w; return w = w ^ (w >> 19) ^ t ^ (t >> 8); }
        /// <summary>
        /// [0,m)
        /// </summary>
        public long NextLong(long m)
        {
            return (long)((((1UL * next()) << 32) + next()) % (ulong)m);
        }
        /// <summary>
        /// [0,m)
        /// </summary>
        public int NextInt(int m) { return (int)(next() % m); }
        /// <summary>
        /// [min,max]
        /// </summary>
        public long NextLong(long min, long max) { return min + NextLong(max - min + 1); }
        /// <summary>
        /// [min,max]
        /// </summary>
        public int NextInt(int min, int max) { return min + NextInt(max - min + 1); }

        /// <summary>
        /// [0,1)
        /// </summary>
        public double NextDouble() { return (double)next() / uint.MaxValue; }
        /// <summary>
        /// pow(a,x). x ∈ [0,1)
        /// </summary>
        public double NextDoubleP(double a) { return Pow(a, NextDouble()); }

    }

}
