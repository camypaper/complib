using System.Collections.Generic;
using CompLib.Collections;
using Xunit;
using Xunit.Abstractions;

namespace CompLib.UnitTest.Collections
{
    public class DisjointSetTest: UnitTestBase
    {
        public DisjointSetTest(ITestOutputHelper output) : base(output)
        {
        }

        [Fact(DisplayName = "Init Test")]
        public void InitTest()
        {
            var s = new DisjointSet(100000);
            for (int i = 0; i < 100000; i++)
                Assert.Equal(i, s[i]);
        }

        [Fact(DisplayName = "Unite Test")]
        public void UniteTest()
        {
            var s = new DisjointSet(5);
            Assert.False(s.Same(0, 2));
            s.Unite(0, 2);
            Assert.True(s.Same(0, 2));

            Assert.False(s.Same(1, 3));
            s.Unite(1, 3);
            Assert.True(s.Same(1, 3));
            Assert.False(s.Same(0, 1));

            Assert.False(s.Same(3, 4));
            s.Unite(3, 4);
            Assert.True(s.Same(1, 4));
            Assert.False(s.Same(0, 4));
        }

        [Fact(DisplayName = "Random Test Small")]
        public void RandomSmallTest()
        {
            const int T = 50;
            for (int _ = 0; _ < T; _++)
            {
                var n = Rand.NextInt(50, 200);
                var s = new DisjointSet(n);
                var t = new UnionSet(n);

                var q = Rand.NextInt(50, 100);
                for (int i = 0; i < q; i++)
                {
                    var x = Rand.NextInt(n);
                    var y = Rand.NextInt(n);
                    Assert.Equal(t.Size(x), s.Size(x));
                    Assert.Equal(t.Size(y), s.Size(y));
                    Assert.Equal(t.Same(x, y), s.Same(x, y));

                    var u = s.Unite(x, y);
                    var v = t.Unite(x, y);

                    Assert.Equal(v, u);
                }
            }
        }

        [Fact(DisplayName = "Random Test Large")]
        public void RandomLargeTest()
        {
            const int T = 5;
            for (int _ = 0; _ < T; _++)
            {
                var n = Rand.NextInt(10000, 100000);
                var s = new DisjointSet(n);
                var t = new UnionSet(n);

                var q = Rand.NextInt(10000, 100000);
                for (int i = 0; i < q; i++)
                {
                    var x = Rand.NextInt(n);
                    var y = Rand.NextInt(n);
                    Assert.Equal(t.Size(x), s.Size(x));
                    Assert.Equal(t.Size(y), s.Size(y));
                    Assert.Equal(t.Same(x, y), s.Same(x, y));

                    var u = s.Unite(x, y);
                    var v = t.Unite(x, y);

                    Assert.Equal(v, u);
                }
            }
        }
        class UnionSet
        {
            List<int>[] s;
            public UnionSet(int n)
            {
                s = new List<int>[n];
                for (int i = 0; i < n; i++)
                    s[i] = new List<int>() { i };
            }
            public bool Unite(int x, int y)
            {
                if (s[x] == s[y]) return false;
                if (s[x].Count < s[y].Count) { var tmp = x; x = y; y = tmp; }
                foreach (var v in s[y]) { s[v] = s[x]; s[x].Add(v); }
                return true;
            }
            public int Size(int x) => s[x].Count;
            public bool Same(int x, int y) => s[x] == s[y];
        }
    }
}
