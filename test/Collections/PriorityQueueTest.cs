﻿using System;
using System.Collections.Generic;
using CompLib.Collections;
using Xunit;
using Xunit.Abstractions;
using static System.Math;

namespace CompLib.UnitTest.Collections
{
    public class PriorityQueueUnitTest: UnitTestBase
    {
        readonly PriorityQueue<int> pq;
        public PriorityQueueUnitTest(ITestOutputHelper output) : base(output)
        {
            pq = new PriorityQueue<int>();
        }

        [Trait("Category", "Data Structure")]
        [Trait("Category", "PriorityQueue")]
        [Fact(DisplayName = "Empty Peek (throw IndexOutOfRangeException)")]
        public void EmptyPeek()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => pq.Peek());
        }
        [Trait("Category", "Data Structure")]
        [Trait("Category", "PriorityQueue")]
        [Fact(DisplayName = "Empty Deque (throw IndexOutOfRangeException")]
        public void EmptyDeque()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => pq.Dequeue());
        }
        [Trait("Category", "Data Structure")]
        [Trait("Category", "PriorityQueue")]
        [Fact(DisplayName = "pq.Any()")]
        public void AnyTest()
        {
            Assert.False(pq.Any());
            pq.Enqueue(1);
            Assert.True(pq.Any());
            pq.Dequeue();
            Assert.False(pq.Any());
        }
        [Trait("Category", "Data Structure")]
        [Trait("Category", "PriorityQueue")]
        [Fact(DisplayName = "Enqueue Dequeue Test")]
        public void EnqueDequeTest()
        {
            var cnt = 0;
            for (int i = 0; i < 100000; i++)
            {
                if (Rand.NextDouble() < 0.5) { cnt++; pq.Enqueue(Rand.NextInt(1, (int)ten(9))); }
                else
                {
                    cnt = Max(0, cnt - 1);
                    if (pq.Count > 0)
                        pq.Dequeue();
                }
                Assert.Equal(cnt, pq.Count);
            }
        }


        [Trait("Category", "Data Structure")]
        [Trait("Category", "PriorityQueue")]
        [Theory(DisplayName = "Random Test, 1<=k,n<=200000, 1<= a_i <= 10^5")]
        [
            InlineData(1, 1),
            InlineData(50, 10),
            InlineData(2000, 50),
            InlineData(2000, 200),
            InlineData(2000, 500),
            InlineData(200000, 1),
            InlineData(200000, 10),
            InlineData(200000, 100),
            InlineData(200000, 1000)
        ]
        public void RandomTest(int n, int k)
        {
            var set = new SortedSet<long>();
            for (int i = 0; i < n; i++)
            {
                var x = Rand.NextInt(1, 100000);
                pq.Enqueue(x);
                set.Add(BigMul(x, n) + i);
                if (pq.Count > k)
                {
                    var u = pq.Dequeue();
                    var v = set.Min;
                    set.Remove(v);
                    Assert.Equal(u, v / n);
                }
            }

        }
    }
}
