﻿using System;
using CompLib.Collections;
using Xunit;
using Xunit.Abstractions;

namespace CompLib.UnitTest.Collections
{
    public class DequeUnitTest: UnitTestBase
    {
        readonly Deque<int> deq;
        public DequeUnitTest(ITestOutputHelper output) : base(output)
        {
            deq = new Deque<int>();
        }

        [Trait("Category", "Data Structure")]
        [Trait("Category", "PopFront")]
        [Fact(DisplayName = "Empty PopFront (throw IndexOutOfRangeException)")]
        public void EmptyPopFront()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => deq.PopFront());
        }
        [Trait("Category", "Data Structure")]
        [Trait("Category", "Deque")]
        [Fact(DisplayName = "Empty PopBack (throw IndexOutOfRangeException")]
        public void EmptyDeque()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => deq.PopBack());
        }
        [Trait("Category", "Data Structure")]
        [Trait("Category", "Dequeue")]
        [Fact(DisplayName = "deq.Any()")]
        public void AnyTest()
        {
            Assert.False(deq.Any());
            deq.PushBack(1);
            Assert.True(deq.Any());
            deq.PopFront();
            Assert.False(deq.Any());
        }
        [Trait("Category", "Data Structure")]
        [Trait("Category", "Dequeue")]
        [Fact(DisplayName = "deq[i]")]
        public void IndexerTest()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => deq[0]);
            deq.PushBack(1);
            Assert.Equal(1, deq[0]);
            deq.PopFront();
            Assert.Throws<ArgumentOutOfRangeException>(() => deq[0]);
        }
        [Trait("Category", "Data Structure")]
        [Trait("Category", "Deque")]
        [Fact(DisplayName = "Enqueue Dequeue Test")]
        public void EnqueDequeTest()
        {
            var ans = new ReferenceDeque<int>();
            for (int i = 0; i < 200000; i++)
            {
                var p = Rand.NextDouble();
                if (p < 1.0 / 3)
                {
                    var x = Rand.NextInt(1, (int)ten(9));

                    if (Rand.NextDouble() < 0.5)
                    {
                        deq.PushBack(x);
                        ans.PushBack(x);
                    }
                    else
                    {
                        deq.PushFront(x);
                        ans.PushFront(x);
                    }
                }
                else if (p < 2.0 / 3)
                {
                    if (deq.Count > 0)
                    {
                        if (Rand.NextDouble() < 0.5)
                            Assert.Equal(ans.PopBack(), deq.PopBack());
                        else Assert.Equal(ans.PopFront(), deq.PopFront());
                    }
                }
                else if (ans.Count > 0)
                {
                    var k = Rand.NextInt(ans.Count);
                    Assert.Equal(ans[k], deq[k]);
                }
                Assert.Equal(ans.Count, deq.Count);
            }
        }
        [Trait("Category", "Data Structure")]
        [Trait("Category", "Deque")]
        [Fact(DisplayName = "Insert RemoveAt Test")]
        public void InsertRemoveTest()
        {
            var ans = new ReferenceDeque<int>();
            for (int i = 0; i < 5000; i++)
            {
                var p = Rand.NextDouble();
                if (p < 1.0 / 3)
                {
                    var k = Rand.NextInt(0, ans.Count);
                    var x = Rand.NextInt(1, (int)ten(9));

                    deq.Insert(x, k);
                    ans.Insert(x, k);
                }
                else if (p < 2.0 / 3)
                {
                    if (deq.Count > 0)
                    {
                        var k = Rand.NextInt(ans.Count);
                        deq.RemoveAt(k);
                        ans.RemoveAt(k);
                    }
                }
                else if (ans.Count > 0)
                {
                    var k = Rand.NextInt(ans.Count);
                    Assert.Equal(ans[k], deq[k]);
                }
                Assert.Equal(ans.Count, deq.Count);
            }
        }


    }
    #region Deque<T>

    internal class ReferenceDeque<T>
    {
        T[] buf;
        int offset;
        int capacity;
        public int Count { get; private set; }
        public ReferenceDeque(int cap) { buf = new T[capacity = cap]; }
        public ReferenceDeque() { buf = new T[capacity = 8]; }
        public T this[int index]
        {
            get { return buf[getIndex(index)]; }
            set { buf[getIndex(index)] = value; }
        }
        private int getIndex(int index)
        {
            if (index >= capacity)
                throw new IndexOutOfRangeException("out of range");
            var ret = index + offset;
            if (ret >= capacity)
                ret -= capacity;
            return ret;
        }
        public void PushFront(T item)
        {
            if (Count == capacity) Extend();
            if (--offset < 0) offset += buf.Length;
            buf[offset] = item;
            ++Count;
        }
        public T PopFront()
        {
            if (Count == 0)
                throw new InvalidOperationException("collection is empty");
            --Count;
            var ret = buf[offset++];
            if (offset >= capacity) offset -= capacity;
            return ret;
        }
        public void PushBack(T item)
        {
            if (Count == capacity) Extend();
            var id = Count++ + offset;
            if (id >= capacity) id -= capacity;
            buf[id] = item;
        }
        public T PopBack()
        {
            if (Count == 0)
                throw new InvalidOperationException("collection is empty");
            return buf[getIndex(--Count)];
        }
        public void Insert(T item, int index)
        {
            if (index > Count) throw new IndexOutOfRangeException();
            PushFront(item);
            for (int i = 0; i < index; i++)
                this[i] = this[i + 1];
            this[index] = item;
        }
        public T RemoveAt(int index)
        {
            if (index < 0 || index >= Count) throw new IndexOutOfRangeException();
            var ret = this[index];
            for (int i = index; i > 0; i--)
                this[i] = this[i - 1];
            PopFront();
            return ret;
        }
        private void Extend()
        {
            var newBuffer = new T[capacity << 1];
            if (offset > capacity - Count)
            {
                var len = buf.Length - offset;
                Array.Copy(buf, offset, newBuffer, 0, len);
                Array.Copy(buf, 0, newBuffer, len, Count - len);
            }
            else Array.Copy(buf, offset, newBuffer, 0, Count);
            buf = newBuffer;
            offset = 0;
            capacity <<= 1;
        }
        public T[] Items
        {
            get
            {
                var a = new T[Count];
                for (int i = 0; i < Count; i++)
                    a[i] = this[i];
                return a;
            }
        }
    }

    #endregion
}
