# CompLib
C#で記述された競技プログラミング用ライブラリです．



現在のところ以下がサポートされています．
✔がついているものは，単体テストも付属しています．

- 数学
    * ✔GCD
    * ✔LCM
    * ✔ModInt
        * 二項係数
    * 行列
- データ構造
    * ✔優先度付きキュー (Binary Heap)
    * ✔デック
    * ✔素集合データ構造
    * 順序付集合
    * Fenwick Tree
- グラフ
    * 木
        + HL分解 + LCA
        + 重心
    * フロー
        + 最大流(Dinic)
        + 最小費用流(Primal Dual)
    * 有向グラフ
        + 強連結成分分解
    * 無向グラフ
        + 二重辺連結成分分解
- 文字列
    * Suffix Array(SA-IS)
    * KMP
    * Manachar
    * Z Algorithm
- 一般的なアルゴリズム
    * 二分探索 (lower bound, upper bound)
    * SAT solver (https://bitbucket.org/not_522/competitive-programming を移植させて頂きました)

## 使用方法
単にコードを貼り付けて使用することを想定しています．
いくつかのコードには依存関係が存在しています．

## ドキュメント
とりあえずドキュメントコメントをつけ始めました．


## テスト
ライブラリには(いずれ)ユニットテストが全て付属し，verifyしながら開発される予定です．
